# TP_JavaSrcipt

Mes tp en java

> - Auteur : Mathis Dejonghe

## Sommaire

- Introduction
  - [HelloWorld](HelloWorldJS/index.html)
- 1. Variables et Opérateurs en JavaScript
  - [Activité : Calcul de surface](VariablesOperateurs/CalculSurf/index.html)
  - [Exercice 1 : Calcul d'IMC](VariablesOperateurs/Calculimc/index.html)
  - [Exercice 2 : Convertion d'une température](VariablesOperateurs/CalculTemp/index.html)
  
- 2.  Les structures de contrôle en JavaScript
  - [Activité : Conversion Euro Dollars](img_cours/syracyse.dollar.PNG)
  - [Activité : Table de multiplication](img_cours/Multiple7.png)
  - [Activité : Suite de Fibonacci](img_cours/syracyse.fibo.png)
  - [Activité : Nombres triples](img_cours/syracyse.Triple.png)
  - [Activité : Conjecture de Syracuse](img_cours/syracyse.png)
  
- 3.  Les fonctions en JavaScript
  - [Exercice 1 : Calcul du temps de parcours d'un trajet](img_cours/tpsparcours.png)
  - [Exercice 2 : Recherche du nombre de multiples de 3](img_cours/multiple3.png)
  - [Activité IMC : Calcul de l'IMC](img_cours/IMC_FCT.png)
  - [Activité IMC : Calcul de l'IMC fonctions imbriquées](img_cours/imc_fctimbriq.png)
  
- 4. Les Objets en JavaScript
- [Activité IMC : Calcul de l'IMC avec objets](img_cours/imcv1.png)
- [Activité IMC : Calcul de l'IMC avec objets litéraux V2](img_cours/imcv2.png)
- [Activité IMC: prototype ](img_cours/IMCproto.png)
- [Activité : Distinction eleve prof / héritage](img_cours/eleveprof.png)
- 
  
