let taille = 175; //taille en cm
let poids = 100; //poids en kg
let IMC; //variable de retour pour l'IMC
let interpretation_msg; //variable pour afficher l'intérpretation du calcul de l'imc

function calculerIMC(prmTaille, prmPoids) {
    let valIMC; // IMC
    valIMC = poids / ((taille * taille) * 10e-5) //Calcul de l'IMC
    return valIMC;
}

function interpreterIMC(prmIMC) {
    let interpretation = ""; //pour l'interpretation de l'IMC
    //boucle pour l'interprétation de l'IMC et affichage
    if (IMC <= 16.5) {
        interpretation = "Dénutrition";
    }
    else if ((IMC >= 16.5) && (IMC < 18.5)) {
        interpretation = "Maigreur";
    }
    else if ((IMC >= 18.5) && (IMC < 25)) {
        interpretation = "Corpulence normale";
    }
    else if ((IMC >= 25) && (IMC < 30)) {
        interpretation = "Surpoids";
    }
    else if ((IMC >= 30) && (IMC < 35)) {
        interpretation = "Obésité modérée";
    }
    else if ((IMC >= 35) && (IMC < 40)) {
        interpretation = "Obésité sévère'";
    }
    else {
        interpretation = "Obésité morbide";
    }
    return interpretation;
}

//Utilisation des fonctions pour calculer l'IMC
IMC = calculerIMC(taille, poids);
interpretation_msg = interpreterIMC(IMC);

//affichage console
console.log("Calcul de l'IMC :"); //pour faire beau
console.log("- Taille : " + taille + " cm"); //affichage taille
console.log("- Poids : " + poids + " kg"); //affichage poids
console.log("Votre IMC est de : " + IMC.toFixed(1)); //affichage de l'IMC
console.log("Interprétation de l'IMC : " + interpretation_msg); //Affichage de l'interpretation du calcul

