//Déclaration de variables
let valeurLimite = 20; //valeur limite de la fonction

//fonction pour trouver les multiples de 3
function calc_Mult3(prmLimite) {
    let multiples; //variable pour rechercher les multiples 
    let aff = ""; //Affichage

    for (multiples = 0; multiples < prmLimite; multiples++) {

        if (multiples % 3 == 0) {
            aff = aff + " " + multiples; 
        }

    }
    return aff; //retourne affichage
}
//affichage console
console.log("Calcul des multiples de 3 :"); //affichage console pour la présentation
console.log("Valeur limite de la fonction : " + valeurLimite);//affichage de la valeur limite à calculer
console.log("Multiples de 3 : " + calc_Mult3(valeurLimite));//afichage des multiples de 3
