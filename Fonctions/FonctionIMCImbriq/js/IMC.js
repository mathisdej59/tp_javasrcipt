// Déclarations de variables
let taille = 175;   // Taille
let poids = 100;    // Poids
let IMC;    // Valeur de l'IMC
let Interp_IMC; // Interprétation de l'IMC
let valeurIMC; // Valeur de l'IMC
let interpretation = "";    // Interp de l'IMC

function decrire_corpulence(prmTaille, prmPoids) {  // Fct imbriqu
    

    function calculerIMC(prmTaille, prmPoids) {
        valIMC = poids / ((taille * taille) * 10e-5);   // Calcul IMC
        valIMC = valIMC.toFixed(1);
    }

    function interpreterIMC() {
        // On choisit le critère de l'IMC en mettant un si avec comme condition l'IMC
        if (valeurIMC < 16.5) {
            interpretation = "dénutrition"
        }
        else if ((valeurIMC >= 16.5 && valIMC < 18.5)) {
            interpretation = "maigreur"
        }
        else if ((valeurIMC >= 18.5) && (valIMC < 25)) {
            interpretation = "corpulence normale"
        }
        else if ((valeurIMC >= 25) && (valIMC < 30)) {
            interpretation = "surpoids"
        }
        else if ((valeurIMC >= 30) && (valIMC < 35)) {
            interpretation = "obésité modérée"
        }
        else if ((valeurIMC >= 35) && (valIMC < 40)) {
            interpretation = "obésité sévère"
        }
        else {
            interpretation = "obésité morbide"
        }
    }
    // Utilisation des fonctions 
    calculerIMC();
    interpreterIMC();
    // Résultat
    return "Votre IMC est égal(e) à " + valIMC + " : ,vous êtes en état de " + interpretation
}

// Affichage du poids, de la taille et de l'IMC dans la console du navigateur
console.log("Calcul de l'IMC :");
console.log("taille : " + taille + "cm");
console.log("poids : " + poids + "kg");

// Affichage de l'interprétation de l'IMC dans la console du navigateur
console.log(decrire_corpulence(taille, poids));

