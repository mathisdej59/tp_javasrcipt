//Déclaration de variables

let tpsTrajet; //valeur du temps de trajet
let distance = 500;  //distance à parcourir en km
let vitesse = 90; //vitesse en km/h


function calculerTempsParcoursSec(prmVitesse, prmDistance) {
    let temps;  // temps de trajet en sec
    temps = ((prmDistance / prmVitesse) * 3600); //Calculer le temps en fonction de la distance et de la vitesse
    return temps; //valeur de retour temps
}

tpsTrajet = calculerTempsParcoursSec(vitesse, distance); //Calculer le temps de trajet

function convertir_h_m_s(prmTemps) {

    let aff;//Affichage
    let heures; //heures
    let minutes;//minutes

    heures = Math.floor(prmTemps / 3600);
    prmTemps %= 3600;
    minutes = Math.floor(prmTemps / 60);
    prmTemps %= 60;

    aff = heures + "h" + minutes + "m" + prmTemps + "s";

    return aff;
}
//affichage console
console.log("Calcul du temps de parcours d'un trajet :"); //affichage console présentation 
console.log("\t-Vitesse moyenne en km/h : " + vitesse); //affichage console de la vitesse
console.log("\t-Distance à parcourir en kilomètres : " + distance);//affichage console de la distance

//affichage console fonction qui convertit le temps
console.log("\tA " + vitesse + "km/h, une distance de " + distance + " km est parcourue en " + convertir_h_m_s(tpsTrajet));
