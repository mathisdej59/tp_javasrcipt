// Fonction Personne
function Personne(prmNom, prmPrenom, prmAge, prmSexe) {
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
}

Personne.prototype.decrire = function () {   // Description personne 
    let description;
    description =this.prenom + " " + this.nom + " est agé(e) de " + this.age + " ans";
    return description;
}

//Constructeur pour --> professeur
function Professeur(prmNom, prmPrenom, prmAge, prmSexe, prmMatiere) {
    Personne.call(this, prmNom, prmPrenom, prmAge, prmSexe);
    this.matiere = prmMatiere;
}

Professeur.prototype = Object.create(Personne.prototype);   // Heritage constructeur Personne()
Professeur.prototype.constructor = Professeur;  // Initialise propriété du constructeur Professeur

// Nouvelle fct Prof
Professeur.prototype.decrire_plus = function () {
    let description;
    let prefixe;
    if (this.sexe == 'M') { //si prof = homme
        prefixe = 'Mr';
    } else {
        prefixe = 'Mme';  //si prof = femme
    }
    description = prefixe + " " + this.nom + " est professeur de " + this.matiere;
    return description;
}

// Constructeur Eleve 
function Eleve(prmNom, prmPrenom, prmAge, prmSexe) {
    Personne.call(this, prmNom, prmPrenom, prmAge, prmSexe);
}

Eleve.prototype = Object.create(Personne.prototype);   // Heritage constructeur Personne
Eleve.prototype.constructor = Eleve;  // Initialise constructeur Eleve

Eleve.prototype.decrire_plus = function () {   
    let description;
    let prefixe;
    if (this.sexe == 'M') {
        prefixe = 'un';
    } else {
        prefixe = 'une';
    }
    description = this.prenom + " " + this.nom + " est " + prefixe + " en classe de SNIR1";
    return description;
}

// Affichage professeur
let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'M', 'Mathématiques');
console.log(objProfesseur1.decrire());
console.log(objProfesseur1.decrire_plus());

// Affichage élève 
let objEleve1 = new Eleve('Dutillieul', 'Dorian', 18, 'M');
console.log(objEleve1.decrire());
console.log(objEleve1.decrire_plus());
