//Déclaration des variables et des objets
let objetPatient = {    // Objet patient
    nom: 'Dupond', //Nom
    prenom: 'Jean', //Prénom
    taille: 180, // Taille
    age: 30, // Âge 
    poids: 85, // Poids
    sexe: 'masculin',// Sexe
    decrire: function () { // Description du patient selon ses critères
        let description;
        description = "Le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + ", agé de " + this.age + " ans. Mesure " + this.taille * 0.01 + "m et pèse " + this.poids + "kg";
        return description; // Retour de la description du patient
    },

    definir_corpulence: function () {
        let interpretation;
        let valIMC = undefined;
        let poids = this.poids;
        let taille = this.taille;
        function calculer_IMC() {
            valIMC = poids / ((taille * taille) * 10e-5); //Calcul de l'IMC
            valIMC = valIMC.toFixed(2);  
        }
    function interpreter_IMC() {
        // On choisit le critère de l'IMC
        if (valIMC <= 16.5) {
            interpretation = "dénutrition";
        }
        else if ((valIMC >= 16.5) && (valIMC < 18.5)) {
            interpretation = "maigreur";
        }
        else if ((valIMC >= 18.5) && (valIMC < 25)) {
            interpretation = "corpulence normale";
        }
        else if ((valIMC >= 25) && (valIMC < 30)) {
            interpretation = "surpoids";
        }
        else if ((valIMC >= 30) && (valIMC < 35)) {
            interpretation = "obésité modérée";
        }
        else if ((valIMC >= 35) && (valIMC < 40)) {
            interpretation = "obésité sévère";
        }
        else if (valIMC >= 40){ 
            interpretation = "obésité morbide";
        }
        
    }
        calculer_IMC();
        interpreter_IMC();
        return + valIMC + "\nIl est en situation de " + interpretation;
    },

};

//Affichage de la description et de la valeur de l'IMC.
console.log(objetPatient.decrire()); // Affichage description
console.log("L'IMC du patient est de : " + objetPatient.definir_corpulence()); // Affichage console du calcul de l'IMC


