// Déclaration des fonctions 

function Personnage(prmNom, prmNiveau) {
    this.nom = prmNom; //nom personnage
    this.niveau = prmNiveau; //niveau personnage
}
Personnage.prototype.saluer = function () {
    let description; //description perso
    description = this.nom + " vous salue !!";
    return description; //retour description
}

function Guerrier(prmNom, prmNiveau, prmArme) {
    Personnage.call(this, prmNom, prmNiveau);
    this.arme = prmArme; // Arme guerrier
}

Guerrier.prototype = Object.create(Personnage.prototype); // Heritage du constructeur Personnage
Guerrier.prototype.constructor = Guerrier;

// Combattre
Guerrier.prototype.combattre = function () {
    let description; // Description fonction Combattre()
    description = this.nom + " est un guerrier qui se bat avec " + this.arme;
    return description; //retour de la description de la fct combattre
}
    // Création guerrier
    let objGuerrier = new Guerrier('Arthur', 3, 'une épée.');

    // Affichage perso Guerrier
    console.log(objGuerrier.saluer());
    console.log(objGuerrier.combattre());

    // Fct perso magicien
    function Magicien(prmNom, prmNiveau, prmPouvoir) {
        Personnage.call(this, prmNom, prmNiveau);
        this.pouvoir = prmPouvoir; //pouvoir du magicien
    }
    Magicien.prototype = Object.create(Personnage.prototype); // Héritage du constructeur Personnage
    Magicien.prototype.constructor = Magicien; // Attribution 

    // Fonction posseder
    Magicien.prototype.posseder = function () {
        let description; // Description fct Posseder
        description = this.nom + " est un magicien qui possède le pouvoir de " + this.pouvoir;
        return description; // Retour description de fct posseder
    }

    // Création du magicien
    let objMagicien = new Magicien('Merlin', 2, 'prédire les batailles.');

    // Affichage sur l'écran du personnage magicien
    console.log(objMagicien.saluer());
    console.log(objMagicien.posseder());
