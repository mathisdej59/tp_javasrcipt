//Déclaration des variables et des objets
let objetPatient = {    // Objet patient
    nom: 'Dupond', //Nom
    prenom: 'Jean', //Prénom
    taille: 180, // Taille
    age: 30, // Âge 
    poids: 85, // Poids
    sexe: 'masculin',// Sexe
     
    
    valIMC: undefined,
    decrire: function () { // Description du patient selon ses critères
        let description;
        description = "Le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + ", agé de " + this.age + " ans. Mesure " + this.taille * 0.01 + "m et pèse " + this.poids + "kg";
        return description; // Retour de la description du patient
    },
    calculer_IMC: function () {
        valIMC = this.poids / ((this.taille * this.taille) * 10e-5);   //Calcul de l'IMC
        this.valIMC = valIMC.toFixed(2);
        return this.valIMC; //on retourne la valeur de l'IMC
    },
    interpreter_IMC: function () {
        // On choisit le critère de l'IMC
        if (this.valIMC <= 16.5) {
            interpretation = "dénutrition";
        }
        else if ((this.valIMC >= 16.5 && this.valIMC < 18.5)) {
            interpretation = "maigreur";
        }
        else if ((this.valIMC >= 18.5) && (this.valIMC < 25)) {
            interpretation = "corpulence normale";
        }
        else if ((this.valIMC >= 25) && (this.valIMC < 30)) {
            interpretation = "surpoids";
        }
        else if ((this.valIMC >= 30) && (this.valIMC < 35)) {
            interpretation = "obésité modérée";
        }
        else if ((this.valIMC >= 35) && (this.valIMC < 40)) {
            interpretation = "obésité sévère";
        }
        else {
            interpretation = "obésité Morbide";
        }
        return interpretation;
    }
};

//Affichage de la description et de la valeur de l'IMC.
console.log(objetPatient.decrire()); // Affichage description
console.log("L'IMC du patient est de : " + objetPatient.calculer_IMC()); // Affichage console du calcul de l'IMC
console.log("Le patient est en situation de " + objetPatient.interpreter_IMC()); // Affichage console de l'état du patient
