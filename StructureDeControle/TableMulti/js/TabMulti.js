const multiple = 7; //const du multi de 7
let resultat = 0; //résultat de la table
let compteur = 0 ;  //compteur boucle calcul table de 7
let mult3 = "" ; //Afficher une * si multiple de 3

for (compteur = 1; compteur <= 20; compteur++) {    //boucle calculs multiple de 7

    resultat = multiple * compteur ;

    if (compteur % 3 == 0) { // afficher * lorsque le nombre est un multiple de 3 

        mult3 = " *";     //affiche * entre chaque multiple de 3
        
    }else {
        mult3 = "";
    }

    console.log(multiple + "*" + compteur + "=" + resultat + mult3 );

   
}
 